let analysisButtonStatus = false;


$("#videoInput").change(function () {
    let src = URL.createObjectURL(document.getElementById('videoInput').files[0]);
    $("video").attr("src", src);
    let path = document.getElementById('videoInput').value.split("\\");
    __global_video_file_name__ = path[path.length - 1];
    __global_main_video_tag__.onloadedmetadata = function (e) {
        init_connection();
        __global_video_duration__ = __global_main_video_tag__.duration;
        __global_viewport_capacity__ = __global_video_duration__;
        $("#controlCenter .control-buttons button[duty|='imformation']").prop("disabled", true);
        $("#controlCenter .control-buttons button[duty|='save']").prop("disabled", true);
        $("#controlCenter .control-buttons button[duty|='start']").prop("disabled", false);
        $("#controlCenter .control-buttons button[duty|='report']").prop("disabled", false);
        render();
    }
});

$("#controlCenter .control-buttons button[duty|='import']").click(function () {
    $("#videoInput").click();
});

$("#controlCenter .control-buttons button[duty|='start']").click(function () {
    if (!analysisButtonStatus) {
        unmount_fe();
        unmount_log();
        unmount_fv();
        __global_features_list__ = __global_features_pre_list__;
        start_analysis_handler();
    } else if (window.confirm("Are you sure you want to stop analysis?")) {
        stop_analysis_handler();
    }
});

$("#controlCenter .control-buttons button[duty|='report']").click(function () {
    $("#reportInput").click();
});

$("#controlCenter .control-buttons button[duty|='save']").click(function () {
    save_report();
});

$("#reportInput").change(function () {
    let files = document.getElementById('reportInput').files;
    let index = 0;
    if (files.length > 0) {
        $("#controlCenter .control-buttons button[duty|='import']").prop("disabled", true);
        $("#controlCenter .control-buttons button[duty|='start']").prop("disabled", true);
        $("#controlCenter .control-buttons button[duty|='report']").prop("disabled", true);
        $("#firstRow #controlCenter .ratio-keeper .loading").removeClass("d-none");
        unmount_fe();
        unmount_log();
        unmount_fv();
        openDB().then(function () {
            __global_data_source_csv__ = true;
            let import_loop = function () {
                csvImport(files[index]).then(() => {
                    if (index === files.length - 1) {
                        __global_data_show_permision__ = true;
                        $("#controlCenter .control-buttons button[duty|='start']").prop("disabled", true);
                        $("#controlCenter .control-buttons button[duty|='report']").prop("disabled", true);
                        $("#controlCenter .control-buttons button[duty|='import']").prop("disabled", false);
                        $("#firstRow #controlCenter .ratio-keeper .loading").addClass("d-none");
                        analysis_progress(0);
                        features_error_rendering();
                        render_error_timeline();
                        fe_set_range_to_view(0, __global_viewport_capacity__);
                        add_error_logs(10);
                    } else {
                        index++;
                        import_loop();
                    }
                });
            }
            import_loop(index);
        });
    }
});

function start_analysis_handler() {
    if (__global_features_list__.length > 0) {
        analysisButtonStatus = true;
        openDB().then(function () {
            send_inital_message()
        })
        $("#controlCenter .control-buttons button[duty|='import']").prop("disabled", true);
        $("#controlCenter .control-buttons button[duty|='start']").text("Stop Analysis");
        $("#controlCenter .control-buttons button[duty|='report']").prop("disabled", true);
        $("#firstRow #controlCenter .ratio-keeper .loading").removeClass("d-none");
        history_obj_creator();
    } else {
        alert("first select some features");
    }
}

function end_of_analysis_handler() {
    analysisButtonStatus = false;
    if (!__global_data_source_csv__) {
        $("#controlCenter .control-buttons button[duty|='save']").prop("disabled", false);
    }
    $("#controlCenter .control-buttons button[duty|='start']").text("Start Analysis");
    $("#controlCenter .control-buttons button[duty|='start']").prop("disabled", false);
    $("#controlCenter .control-buttons button[duty|='import']").prop("disabled", false);
    $("#firstRow #controlCenter .ratio-keeper .loading").addClass("d-none");
    analysis_progress(0)
}

function stop_analysis_handler() {
    analysisButtonStatus = false;
    init_connection();
    $("#controlCenter .control-buttons button[duty|='start']").text("Start Analysis");
    $("#controlCenter .control-buttons button[duty|='report']").prop("disabled", false);
    $("#controlCenter .control-buttons button[duty|='import']").prop("disabled", false);
    $("#firstRow #controlCenter .ratio-keeper .loading").addClass("d-none");
    analysis_progress(0)
}


function analysis_progress(time) {
    let progress = Number(time) / __global_video_duration__ * 100;
    progress = progress > 100 ? 100 : progress;
    $("#firstRow #controlCenter .ratio-keeper .loading p.title").text("Analysing (" + Math.floor(progress) + "%)");
    $("#firstRow #controlCenter .ratio-keeper .loading .progress .active").css("width", progress + "%")
}




function csvImport(file) {
    return new Promise((resolve, reject) => {
        let reader = new FileReader();
        reader.readAsText(file);
        reader.onload = function (event) {
            let allTextLines = event.target.result.split(/\r\n|\n/);
            let keys = allTextLines[0].split(/,|;/);
            __global_features_list__ = [];
            for (let i = 1; i < keys.length; i++) {
                // $("#sideBarVideo .feature[key|='" + keys[i] + "'] input").prop("checked", true);
                if (__global_features_pre_list__.includes(keys[i])) {
                    __global_features_list__.push(keys[i]);
                }
            }
            history_obj_creator();
            let index = 1;
            let data_importer = function () {
                data = allTextLines[index].split(/,|;/);
                let record = {
                    type: "log",
                    data_time: Number(data[0])
                }
                let features = []
                for (let j = 1; j < data.length; j++) {
                    features.push({
                        key: keys[j],
                        value: data[j]
                    });
                }
                record.data = features;
                sotore_data_in_db(record).then(() => {
                    analysis_progress(data[0]);
                    if (index === allTextLines.length - 2) {
                        resolve(true);
                    } else {
                        index++;
                        data_importer();
                    }
                });
            }
            data_importer();
        };
    })

}


function add_video_imformation(data) {
    for (let i = 0; i < data.length; i++) {
        let element = $("#informationModal table tr.source").clone();
        element.removeClass("source");
        element.find(".key").text(data[i].key);
        element.find(".value").text(data[i].value);
        element.insertBefore("#informationModal table tr.source");
    }
    $("#controlCenter .control-buttons button[duty|='imformation']").prop("disabled", false);
}


function save_report() {
    let index = 1;
    let result = [];
    let name_date = new Date();
    let bridge_func = function (returend_data) {
        let data = returend_data.data.filter((item) => (__global_features_list__.includes(item.key)));
        let row = [];
        row.push(returend_data.data_time)
        for (let i = 0; i < data.length; i++) {
            row.push(data[i].value);
        }
        result.push(row);
        if (result.length >= __csv_file_capacity__) {
            csv_generator(result, index, name_date);
            index++;
            result = [];
        }
        return true;
    }
    return new Promise(function (resolve, reject) {
        remote_get_data_from_db(0, bridge_func, false).then(function (staus) {
            if (result.length > 0) {
                csv_generator(result, index, name_date);
                index++;
                result = [];
            }
            resolve(true);
        });
    });
}

function csv_generator(content, index, name_date) {
    let csvContent = "data:text/csv;charset=utf-8,";
    csvContent += ["time", ...__global_features_list__].join(",") + "\r\n";
    let tempContent = ""
    content.forEach(function (rowArray) {
        let row = rowArray.join(",");
        tempContent += row + "\r\n";
    });
    if (navigator.msSaveOrOpenBlob) {
        // Works for Internet Explorer and Microsoft Edge
        let blob = new Blob([tempContent], { type: "text/csv" });
        navigator.msSaveOrOpenBlob(blob, __export_pre_name__ + "-" + __global_video_file_name__ + "-" + index + '.csv');
    } else {
        csvContent += tempContent
        var hiddenElement = document.createElement('a');
        hiddenElement.href = encodeURI(csvContent);
        hiddenElement.target = '_blank';
        hiddenElement.download = __export_pre_name__ + "-" + __global_video_file_name__ + "-" + name_date + "-" + index + '.csv';
        document.getElementById("firstRow").appendChild(hiddenElement);
        console.log(hiddenElement);
        hiddenElement.click();
    }
}