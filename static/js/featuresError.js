let active_features = [];
let min_barcode_width = 2;
let num_of_barcode_sections = 500;

let chart_options = {
    tooltips: {
        enabled: false,
    },
    legend: {
        display: false
    },
    elements: {
        point: {
            radius: 0
        }
    },
    maintainAspectRatio: false,
    scales: {
        yAxes: [{
            ticks: {
                min: 0,
                max: 1,
                stepSize: 0.25
            },
            gridLines: {
                scaleLabel: false,
                zeroLineColor: __global_theme__.primay,
                color: "rgba(255,255,255,0.2)"
            },
        }],
        xAxes: [{
            ticks: {
                display: false,
                min: 0,
                max: 100,
                stepSize: 5
            },
            scaleLabel: {
                display: false,
            },
            gridLines: {
                display: false
            },
            type: 'linear',
            position: 'bottom',
        }]
    }
}

function unmount_fe(){
    $(".features-error:not(.source)").remove();
    $("#featuresErrorSectionTitile").addClass("d-none")
}


function features_error_rendering() {
    active_features = [];
    unmount_fe();
    $(".features-error.source .error-content .scroll-manager .scroll-body").css("width", __global_timeline_content_width__ + "px");
    __global_features_list__.length ? $("#featuresErrorSectionTitile").removeClass("d-none") : null;
    let gradientFill = document.getElementsByTagName("canvas")[0].getContext("2d").createLinearGradient(0, 70, 0, 200);
    gradientFill.addColorStop(0, __global_theme__.primay);
    gradientFill.addColorStop(1, __global_theme__.secondary);
    for (let i = 0; i < __global_features_list__.length; i++) {
        let source = $(".features-error.source").clone();
        let id = "fe" + __global_features_list__[i];
        source.removeClass("source");
        source.attr("id", id);
        source.find(".expand-controler div").click(function () { fe_click_handler(id) });
        source.find(".expand-controler span").text(__global_features_list__[i]);
        let ctx = source.find("canvas");
        chart_options.scales.xAxes[0].ticks.max = __global_timeline_content_width__
        let diagram = new Chart(ctx, {
            type: 'line',
            data: {
                datasets: [{
                    data: [],
                    backgroundColor: gradientFill
                }]
            },
            options: chart_options
        });
        diagram.canvas.parentNode.style.width = __global_timeline_content_width__ + 'px';
        source.insertBefore($(".features-error.source"));
        active_features.push({
            id: id,
            diagram: diagram,
            expand: false,
            nodes_num: 0,
            nodes_sum: 0,
            pre_time: 0
        });
    }
    $(".features-error .error-content .scroll-manager .cursor").css("left", -$(".features-error .error-content .scroll-manager .cursor").width() / 2 + "px")
}

function setThemeToCharts() {
    let gradientFill = document.getElementsByTagName("canvas")[0].getContext("2d").createLinearGradient(0, 70, 0, 200);
    gradientFill.addColorStop(0, __global_theme__.primay);
    gradientFill.addColorStop(1, __global_theme__.secondary);
    for (let i = 0; i < active_features.length; i++) {
        active_features[i].diagram.data.datasets[0].backgroundColor = gradientFill;
        active_features[i].diagram.update();
    }
}

async function clean_fe_view() {
    for (let i = 0; i < active_features.length; i++) {
        $("#" + active_features[i].id + " .error-content .scroll-manager .barcode-keeper .error:not(.source)").remove();
        active_features[i].diagram.data.datasets[0].data = [];
        active_features[i].diagram.update();
        active_features[i].nodes_num = 0;
        active_features[i].nodes_sum = 0;
        active_features[i].pre_time = 0;
    }
}
function fe_set_range_to_view(start, end) {
    clean_fe_view();
    initial_generator_history();
    let bridge_func = function (returend_data) {
        fe_add_log_to_diagram(returend_data);
        if (returend_data.data_time >= end) {
            fe_add_log_to_barcode(returend_data, true);
            initial_generator_history();
            return false
        } else {
            fe_add_log_to_barcode(returend_data, false);
            return true
        }
    }
    remote_get_data_from_db(start, bridge_func, false);
}




function fe_add_log_to_diagram(log) {
    for (let i = 0; i < log.data.length; i++) {
        if (__global_features_list__.includes(log.data[i].key)) {
            fe_add_diagram_data(log.data[i].key, log.data_time, log.data[i].value);
        }
    }
}

function fe_add_diagram_data(feature_name, time, value) {
    let id = "fe" + feature_name;
    let feature = null;
    for (let i = 0; i < active_features.length; i++) {
        if (active_features[i].id == id) {
            feature = active_features[i];
        }
    }
    if (feature != null) {
        let diff = time - feature.pre_time;
        let min_diff = __global_viewport_capacity__ / __num_of_error_timeline_capacity__;
        if (diff >= min_diff) {
            let new_value = (feature.nodes_sum + value) / (feature.nodes_num + 1);
            feature.diagram.data.datasets[0].data.push({
                x: time * __global_timeline_content_width__ / __global_video_duration__,
                y: new_value
            });
            feature.diagram.update();
            feature.nodes_num = 0;
            feature.nodes_sum = 0;
            feature.pre_time = time;
        }
    }
}


let log_generator_history = new Object();

function initial_generator_history() {
    log_generator_history = new Object();
    for (let i = 0; i < __global_features_list__.length; i++) {
        log_generator_history[__global_features_list__[i]] = null;
    }
}

function fe_add_log_to_barcode(log, isLast) {
    for (let i = 0; i < log.data.length; i++) {
        if (log_generator_history[log.data[i].key] || log_generator_history[log.data[i].key] === null) {
            let treshold = __global_setting__.find((item) => item.key === log.data[i].key).treshold;
            if (log.data[i].value >= treshold && log_generator_history[log.data[i].key] === null) {
                log_generator_history[log.data[i].key] = log.data_time;
            } else if (log.data[i].value >= treshold && log.data_time - log_generator_history[log.data[i].key] > __log_waiting_time__) {
                fe_add_barcode_data(log.data[i].key, log_generator_history[log.data[i].key], log.data_time)
                log_generator_history[log.data[i].key] = null;
            } else if (log.data[i].value >= treshold && isLast) {
                fe_add_barcode_data(log.data[i].key, log_generator_history[log.data[i].key], log.data_time)
            }
            else if (log.data[i].value < treshold && log_generator_history[log.data[i].key] !== null) {
                fe_add_barcode_data(log.data[i].key, log_generator_history[log.data[i].key], log.data_time);
                log_generator_history[log.data[i].key] = null;
            }
        }
    }
}


function fe_add_barcode_data(feature_name, start, end) {
    let id = "fe" + feature_name;
    let diff = end - start;
    let min_diff = __global_viewport_capacity__ / __num_of_error_timeline_capacity__;
    min_diff = 0;
    if (diff >= min_diff) {
        let elem = $("#" + id + " .error-content .scroll-manager .barcode-keeper .error.source").clone();
        elem.removeClass("source");
        elem.css("left", (start * __global_timeline_content_width__ / __global_video_duration__) + "px");
        elem.css("width", (diff * __global_timeline_content_width__ / __global_video_duration__) + "px")
        elem.insertBefore("#" + id + " .error-content .scroll-manager .barcode-keeper .error.source");
    }

}

function fe_click_handler(id) {
    let index = 0;
    for (let i = 0; i < active_features.length; i++) {
        if (active_features[i].id === id) {
            index = i;
            break;
        }
    }
    let status = active_features[index].expand;
    if (!status) {
        $("#" + id + " .cursor span").css("height", 140 + "px");
        $("#" + id).css("height", 190 + "px");
        $("#" + id).find(".error-content .barcode-keeper").css("margin-top", -50 + "px");
        $("#" + id).find(".expand-controler i").text("remove");
        active_features[index].expand = true;
    } else {
        $("#" + id + " .cursor span").css("height", 35 + "px");
        $("#" + id).css("height", 70 + "px");
        $("#" + id).find(".error-content .barcode-keeper").css("margin-top", 10 + "px");
        $("#" + id).find(".expand-controler i").text("add");
        active_features[index].expand = false;
    }
}


