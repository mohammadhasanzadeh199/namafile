let frame_height = 100;
let min_space_between_frames = 80;
let min_space_between_indexes = 100;

let wavesurfer = null;
let wavesurfer2 = null;

function render() {
    $("#timeline").removeClass("source");
    // -------- remove pre elements from last render ----------------------------------
    $("#timeline .timeline-container .frame-keeper video:not(.source)").remove();
    $("#timeline .timeline-container .time-index h6:not(.source)").remove();
    $("#waveform").empty();
    $("#waveform2").empty();
    // -------- render frames ---------------------------------------------------------
    initialize_timeline_buttons();
    width = __global_main_video_tag__.videoWidth;
    height = __global_main_video_tag__.videoHeight;
    let container_width = $("#timeline .timeline-container .timeline-viewport").width();
    __global_timeline_content_width__ = __global_video_duration__ / __global_viewport_capacity__ * container_width;
    let frame_width = frame_height * width / height;
    let frames_num = Math.floor((__global_timeline_content_width__ + min_space_between_frames) / (frame_width + min_space_between_frames));
    min_space_between_frames = (__global_timeline_content_width__ - (frames_num * frame_width)) / (frames_num - 1);
    let time_interval = __global_video_duration__ * (min_space_between_frames + frame_width) / (__global_timeline_content_width__);
    $("#timeline .timeline-container .frame-keeper .source").css({ "width": frame_width + "px", "height": frame_height + "px" });
    let time = frame_width / 2 / __global_timeline_content_width__ * __global_video_duration__;
    for (let i = 0; i < frames_num; i++) {
        let vid = $("#timeline .timeline-container .frame-keeper .source").clone();
        vid.removeClass("source");
        if (i) {
            vid.css("margin-left", min_space_between_frames + "px");
        }
        vid.attr("id", "fameShow" + i)
        vid.insertBefore($("#timeline .timeline-container .frame-keeper .source"))
        document.getElementById("fameShow" + i).currentTime = time;
        time += time_interval;
    }
    // -------- rendoer time indexes --------------------------------------------------
    let source_width = 64;
    let num_of_indexes = Math.floor((__global_timeline_content_width__ - source_width + min_space_between_indexes) / (source_width + min_space_between_indexes));
    min_space_between_indexes += (__global_timeline_content_width__ - (num_of_indexes + 1) * (source_width + min_space_between_indexes)) / (num_of_indexes + 1);
    time_interval = __global_video_duration__ * (min_space_between_indexes + source_width) / __global_timeline_content_width__;
    time = time_interval;
    for (let i = 0; i < num_of_indexes; i++) {
        let index = $("#timeline .timeline-container .time-index .source").clone()
        index.removeClass("source");
        index.css({
            "margin-left": min_space_between_indexes + (!i ? source_width / 2 : 0) + 'px',
            "width": source_width + "px"
        });
        index.text(timeWrapper(time));
        time += time_interval;
        index.insertBefore($("#timeline .timeline-container .time-index .source"));

        index = $(".time-progress-bar .timeline-container .time-index .source").clone()
        index.removeClass("source");
        index.css({
            "margin-left": min_space_between_indexes + (!i ? source_width / 2 : 0) + 'px',
            "width": source_width + "px"
        });
        index.text(timeWrapper(time));
        time += time_interval;
        index.insertBefore($(".time-progress-bar .timeline-container .time-index .source"));
    }
    $("#timeline .timeline-container .cursor").css("left", -$("#timeline .timeline-container .cursor").width() / 2 + "px")
    // -------- wave form renderer ----------------------------------------------------
    wavesurfer = WaveSurfer.create({
        container: '#waveform',
        waveColor: __global_theme__.primay,
        progressColor: __global_theme__.primay,
        barHeight: 0.7,
        height: 60,
        cursorWidth: '0'
    });
    wavesurfer2 = WaveSurfer.create({
        container: '#waveform2',
        waveColor: __global_theme__.primay,
        progressColor: __global_theme__.primay,
        barHeight: 0.7,
        height: 60,
        cursorWidth: '0'
    });
    wavesurfer.load(document.getElementsByTagName("video")[0].src);
    wavesurfer2.load(document.getElementsByTagName("video")[0].src);
}

function setSurferTheme() {
    wavesurfer.setProgressColor(__global_theme__.primay);
    wavesurfer.setWaveColor(__global_theme__.primay);
    wavesurfer2.setProgressColor(__global_theme__.primay);
    wavesurfer2.setWaveColor(__global_theme__.primay);
}

function timeWrapper(time) {
    time = Math.floor(time);
    let second = time % 60;
    let min = Math.floor(time / 60) % 60;
    let hour = Math.floor(time / 3600);
    return time_digit(hour) + ":" + time_digit(min) + ":" + time_digit(second);
}


function initialize_timeline_buttons() {
    $("#timeline .interval button").prop("disabled", true);
    $("#timeline .interval button[duration|='all'").prop("disabled", false);
    $("#timeline .interval button").attr("state", "not_selected");
    if (__global_video_duration__ >= 60) {
        $("#timeline .interval button[duration|='1m']").prop("disabled", false);
    }
    if (__global_video_duration__ >= 300) {
        $("#timeline .interval button[duration|='5m']").prop("disabled", false);
    }
    if (__global_video_duration__ >= 900) {
        $("#timeline .interval button[duration|='15m']").prop("disabled", false);
    }
    if (__global_video_duration__ >= 1800) {
        $("#timeline .interval button[duration|='30m']").prop("disabled", false);
    }
    if (__global_video_duration__ >= 3600) {
        $("#timeline .interval button[duration|='1h']").prop("disabled", false);
    }
    switch (__global_viewport_capacity__) {
        case 60:
            $("#timeline .interval button[duration|='1m'").attr("state", "selected"); break;
        case 300:
            $("#timeline .interval button[duration|='5m'").attr("state", "selected"); break;
        case 900:
            $("#timeline .interval button[duration|='15m'").attr("state", "selected"); break;
        case 1800:
            $("#timeline .interval button[duration|='30m'").attr("state", "selected"); break;
        case 3600:
            $("#timeline .interval button[duration|='1h'").attr("state", "selected"); break;
        case __global_video_duration__:
            $("#timeline .interval button[duration|='all'").attr("state", "selected"); break;
    }
}


function time_digit(time_value) {
    if (time_value / 10 < 1) {
        return "0" + time_value;
    } else {
        return time_value;
    }
}

$("#timeline .interval button").click(function () {
    let duration = $(this).attr("duration");
    let vp_duration = __global_viewport_capacity__;
    switch (duration) {
        case "1m":
            vp_duration = 60;
            break;
        case "5m":
            vp_duration = 300;
            break;
        case "15m":
            vp_duration = 900;
            break;
        case "30m":
            vp_duration = 1800;
            break;
        case "1h":
            vp_duration = 3600;
            break;
        case "all":
            vp_duration = __global_video_duration__;
            break;
    }
    if (__global_video_duration__ === null) {
        alert("first open video");
    } else if (vp_duration > __global_video_duration__) {
        alert("it's wrong");
    } else if (vp_duration != __global_viewport_capacity__) {
        __global_viewport_capacity__ = vp_duration;
        render();
        if (__global_data_show_permision__) {
            features_error_rendering();
            render_error_timeline();
            fe_set_range_to_view(0, __global_viewport_capacity__);
        }
    }
});