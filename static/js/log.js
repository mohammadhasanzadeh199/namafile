let history_obj = [];

function history_obj_creator() {
    for (let i = 0; i < __global_features_list__.length; i++) {
        history_obj.push({
            key: __global_features_list__[i],
            start: 0,
            error_border: null,
            end: 0,
        })
    }
}

function add_error_logs(num_of_errors) {
    log_extractor(num_of_errors).then(function (result) {
        if (num_of_errors >= 0) {
            if (result.length < num_of_errors) {
                $("#errorLog .scroll-controler .after").addClass("source")
            } else if (result.length = num_of_errors) {
                $("#errorLog .scroll-controler .after").removeClass("source")
            }
            let first_elems = $("#errorLog .scroll-controler .record:not(.source)");
            console.log(first_elems)
            for (let i = 0; i < result.length; i++) {
                if (first_elems.length - 1 >= i) {
                    let start = $(first_elems[i]).attr("end");
                    history_obj[history_obj.map((item) => (item.key)).indexOf($(first_elems[i]).attr("key"))].start = Number(start);
                    $(first_elems[i]).remove();
                    $("#errorLog .scroll-controler .before").removeClass("source");
                }
                let element = $("#errorLog .scroll-controler .record.source").clone();
                element.attr("start", result[i].start);
                element.attr("end", result[i].end);
                element.attr("key", result[i].key);
                element.removeClass("source");
                element.find(".name").text(map_key_to_name(result[i].key));
                element.find(".start").text(Math.floor(result[i].start * 100) / 100);
                element.find(".end").text(Math.floor(result[i].end * 100) / 100);
                element.find(".duration").text(Math.floor((result[i].end - result[i].start) * 100) / 100);
                element.insertBefore("#errorLog .scroll-controler .after");
            }
        } else {
            if (result.length < Math.abs(num_of_errors)) {
                $("#errorLog .scroll-controler .before").addClass("source")
            } else if (result.length = Math.abs(num_of_errors)) {
                $("#errorLog .scroll-controler .before").removeClass("source")
            }
            let list_elems = $("#errorLog .scroll-controler .record:not(.source)");
            let last_elems = [];
            list_elems.map((item) => { last_elems.push(item) });
            for (let i = 0; i < list_elems.length; i++) {
                last_elems.push(list_elems[i]);
            }
            last_elems = last_elems.reverse();
            for (let i = 0; i < result.length; i++) {
                if (last_elems.length - 1 >= i) {
                    let end = $(last_elems[i]).attr("start");
                    history_obj[history_obj.map((item) => (item.key)).indexOf($(last_elems[i]).attr("key"))].end = Number(end);
                    $(last_elems[i]).remove();
                    $("#errorLog .scroll-controler .after").removeClass("source");
                    console.log(end, history_obj)
                }
                let element = $("#errorLog .scroll-controler .record.source").clone();
                element.attr("start", result[i].start);
                element.attr("end", result[i].end);
                element.attr("key", result[i].key);
                element.removeClass("source");
                element.find(".name").text(map_key_to_name(result[i].key));
                element.find(".start").text(Math.floor(result[i].start * 100) / 100);
                element.find(".end").text(Math.floor(result[i].end * 100) / 100);
                element.find(".duration").text(Math.floor((result[i].end - result[i].start) * 100) / 100);
                element.insertAfter("#errorLog .scroll-controler .before");
            }
        }
    })
}

function unmount_log(){
    $("#errorLog .scroll-controler .record:not(.source)").remove();
    $("#errorLogSectionTitle").css("display", "none");
    $("#errorLog").css("display", "none");
}

function render_error_timeline() {
    $("#errorLogSectionTitle").css("display", "block");
    $("#errorLog").css("display", "block");
}


function log_extractor(num_of_data) {
    let start_time;
    let bridge_func;
    let result = [];
    if (num_of_data >= 0) {
        start_time = Math.min(...history_obj.map((item) => item.end));
        bridge_func = function (returend_data) {
            result = data_proccess(returend_data, result, num_of_data);
            if (result.length >= Math.abs(num_of_data)) {
                return false;
            } else {
                return true;
            }
        }
    } else {
        start_time = Math.min(...history_obj.map((item) => item.start));
        bridge_func = function (returend_data) {
            result = data_proccess(returend_data, result, num_of_data);
            if (result.length >= Math.abs(num_of_data)) {
                return false;
            } else {
                return true;
            }
        }
    }
    return new Promise(function (resolve, reject) {
        remote_get_data_from_db(start_time, bridge_func, num_of_data < 0).then(function (staus) {
            if (result.length < Math.abs(num_of_data)) {
                result = get_last_ones(num_of_data, result);
            }
            resolve(result);
        });
    });
}

function get_last_ones(num_of_data, result) {
    let new_result = result;
    if (num_of_data >= 0) {
        for (let i = 0; new_result.length < Math.abs(num_of_data) && i < history_obj.length; i++) {
            if (history_obj[i].error_border !== null) {
                new_result.push({
                    start: history_obj[i].error_border,
                    end: __global_video_duration__,
                    key: history_obj[i].key
                });
                history_obj[i].end = __global_video_duration__;
                history_obj[i].error_border = null;
            }
        }
    } else {
        for (let i = 0; new_result.length < Math.abs(num_of_data) && i < history_obj.length; i++) {
            if (history_obj[i].error_border !== null) {
                new_result.push({
                    end: history_obj[i].error_border,
                    start: 0,
                    key: history_obj[i].key
                });
                history_obj[i].start = 0;
                history_obj[i].error_border = null;
            }
        }
    }
    return new_result;
}


function data_proccess(returend_data, result, num_of_data) {
    let data = returend_data.data.filter((item) => (__global_features_list__.includes(item.key)));
    let new_result = [...result];
    if (num_of_data >= 0) {
        for (let i = 0; i < data.length && new_result.length < num_of_data; i++) {
            let trishold_value = __global_setting__.find((item) => item.key === data[i].key).treshold;
            let index = history_obj.map((item) => (item.key)).indexOf(data[i].key);
            if (returend_data.data_time > history_obj[index].end) {
                if (data[i].value >= trishold_value && history_obj[index].error_border == null) {
                    history_obj[index].error_border = returend_data.data_time;
                } else if (data[i].value >= trishold_value && returend_data.data_time - history_obj[index].error_border >= __log_waiting_time__) {
                    new_result.push({
                        start: history_obj[index].error_border,
                        end: returend_data.data_time,
                        key: data[i].key
                    });
                    history_obj[index].error_border = null;
                    history_obj[index].end = returend_data.data_time;
                } else if (returend_data.data[i].value < trishold_value && history_obj[index].error_border != null) {
                    new_result.push({
                        start: history_obj[index].error_border,
                        end: returend_data.data_time,
                        key: data[i].key
                    });
                    history_obj[index].error_border = null;
                    history_obj[index].end = returend_data.data_time;
                }
            }
        }
    } else {
        for (let i = 0; i < data.length && new_result.length < Math.abs(num_of_data); i++) {
            let index = history_obj.map((item) => (item.key)).indexOf(data[i].key);
            if (returend_data.data_time < history_obj[index].start) {
                if (data[i].value >= trishold_value && history_obj[index].error_border == null) {
                    history_obj[index].error_border = returend_data.data_time;
                    console.log("init", data[i].value, returend_data.data_time);
                } else if (data[i].value >= trishold_value && - returend_data.data_time + history_obj[index].error_border >= __log_waiting_time__) {
                    console.log("long time", data[i].value, returend_data.data_time);
                    new_result.push({
                        start: returend_data.data_time,
                        end: history_obj[index].error_border,
                        key: data[i].key
                    });
                    history_obj[index].error_border = null;
                    history_obj[index].start = returend_data.data_time;
                } else if (returend_data.data[i].value < trishold_value && history_obj[index].error_border != null) {
                    console.log("returned", data[i].value, returend_data.data_time);
                    new_result.push({
                        start: returend_data.data_time,
                        end: history_obj[index].error_border,
                        key: data[i].key
                    });
                    history_obj[index].error_border = null;
                    history_obj[index].start = returend_data.data_time;
                }
            }
        }
    }
    return new_result;
}

$("#errorLog .scroll-controler .after").click(function () {
    add_error_logs(10);
})
$("#errorLog .scroll-controler .before").click(function () {
    add_error_logs(-10);
})