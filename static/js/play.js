var play_interval = null;
let is_timeline_view_scroll_by_system = false;

__global_main_video_tag__.addEventListener("timeupdate", cursor_paly);
__global_main_video_tag__.addEventListener("play", function () {
    is_timeline_view_scroll_by_system = false;
})
__global_main_video_tag__.addEventListener("pause", function () {
})
function cursor_paly(event) {
    let cursor_width = $("#timeline .timeline-container .cursor").width()
    let left = __global_main_video_tag__.currentTime / __global_video_duration__ * __global_timeline_content_width__ - cursor_width / 2;
    play_data()
    $("#timeline .timeline-container .cursor").css("left", left + "px");
    $(".time-progress-bar .timeline-container .cursor").css("left", left + "px");
    $(".features-error:not(.source) .error-content .scroll-manager .cursor").css("left", left + "px");
    auto_scroll_check();
}

$("#timeline .timeline-container .scroll-manager .click-area").click(function (event) {
    let position = (event.pageX - $(this).offset().left) + $(this).scrollLeft();
    let time = position / __global_timeline_content_width__ * __global_video_duration__;
    __global_main_video_tag__.currentTime = time;
})

$(".time-progress-bar .timeline-container .scroll-manager .click-area").click(function (event) {
    let position = (event.pageX - $(this).offset().left) + $(this).scrollLeft();
    let time = position / __global_timeline_content_width__ * __global_video_duration__;
    __global_main_video_tag__.currentTime = time;
})

function play_data() {
    get_closest_data_from_db(__global_main_video_tag__.currentTime).then(function (data) {
        show_features_value(data.data);
    })
}

$("#timeline .timeline-container .timeline-viewport").on("scroll", function () {
    if (is_timeline_view_scroll_by_system) {
        is_timeline_view_scroll_by_system = false;
    } else {
        __global_main_video_tag__.pause();
    }
});

function auto_scroll_check() {
    let cursor_normal_position = 0.5;
    let old_scroll = $("#timeline .timeline-container .timeline-viewport").scrollLeft();
    let view_start_time = old_scroll / __global_timeline_content_width__ * __global_video_duration__;
    let progress = (__global_main_video_tag__.currentTime - view_start_time) / __global_viewport_capacity__;
    let normal_scroll = (__global_main_video_tag__.currentTime - cursor_normal_position * __global_viewport_capacity__) / __global_video_duration__ * __global_timeline_content_width__;
    if (progress > 0.95) {
        is_timeline_view_scroll_by_system = true;
        let limit_scroll = (__global_video_duration__ - __global_viewport_capacity__) / __global_video_duration__ * __global_timeline_content_width__;
        let new_scroll = normal_scroll > limit_scroll ? limit_scroll : normal_scroll;
        if (new_scroll > old_scroll) {
            $(".time-progress-bar .timeline-container .timeline-viewport").scrollLeft(new_scroll);
            $("#timeline .timeline-container .timeline-viewport").scrollLeft(new_scroll);
        }
    } else if (progress < 0) {
        is_timeline_view_scroll_by_system = true;
        let limit_scroll = 0;
        let new_scroll = normal_scroll > limit_scroll ? normal_scroll : limit_scroll;
        if (new_scroll < old_scroll) {
            $("#timeline .timeline-container .timeline-viewport").scrollLeft(new_scroll);
        }
    }
    old_scroll = Number($(".features-error:not(.source) .error-content .scroll-manager .scroll-body").css("left").match(/\d+/)[0]);
    view_start_time = old_scroll / __global_timeline_content_width__ * __global_video_duration__;
    progress = (__global_main_video_tag__.currentTime - view_start_time) / __global_viewport_capacity__;
    if (progress > 0.95) {
        let limit_scroll = (__global_video_duration__ - __global_viewport_capacity__) / __global_video_duration__ * __global_timeline_content_width__;
        let new_scroll = normal_scroll > limit_scroll ? limit_scroll : normal_scroll;
        if (new_scroll > old_scroll) {
            $(".features-error:not(.source) .error-content .scroll-manager .scroll-body").css("left", -new_scroll + "px");
            let new_start = new_scroll * __global_video_duration__ / __global_timeline_content_width__;
            let new_end = new_start + __global_viewport_capacity__;
            fe_set_range_to_view(new_start, new_end);
        }
    } else if (progress < 0) {
        let limit_scroll = 0;
        let new_scroll = normal_scroll > limit_scroll ? normal_scroll : limit_scroll;
        if (new_scroll < old_scroll) {
            $(".features-error:not(.source) .error-content .scroll-manager .scroll-body").css("left", -new_scroll + "px");
            let new_start = new_scroll * __global_video_duration__ / __global_timeline_content_width__;
            let new_end = new_start + __global_viewport_capacity__;
            fe_set_range_to_view(new_start, new_end);
        }
    }
}