//inited

//backgorund
//surface
//line
//text
//icon

let __theme_mode_colors__ = [
    {
        backgorund: "#000000",
        surface: "#404040"
    },
    {
        backgorund: "#ffffff",
        surface: "#e2e2e2"
    }
]
let __theme_colors__ = [
    { primay: "#bd99f9", secondary: "#320387" },
    { primay: "#beeef9", secondary: "#1e3376" },
    { primay: "#ffd8ab", secondary: "#4f4438" },
    { primay: "#b7ffb5", secondary: "#3b4f3a" },
    { primay: "#ffc9d9", secondary: "#78474f" }
]

function init_theme() {
    let theme_mode_index = localStorage.getItem("namafile-theme-mode");
    if (theme_mode_index && theme_mode_index !== null) {
        theme_mode_index = Number(theme_mode_index);
        __global_theme_mode__ = __theme_mode_colors__[theme_mode_index];
        if (theme_mode_index === 1) {
            $("body").attr("theme-mode", "light");
            $("#sideBarTheme .them-mode .big-radio input[value|='light']").prop("checked",true);
        }
    } else {
        localStorage.setItem("namafile-theme-mode", 0);
    }
    let theme_index = localStorage.getItem("namafile-theme");
    if (theme_index && theme_index !== null) {
        theme_index = Number(theme_index);
        __global_theme_mode__ = __theme_mode_colors__[theme_index];
        if (theme_index === 1) {
            $("body").attr("theme", "ice-blue");
            $("#sideBarTheme .custom-radio input[value|='ice blue']").prop("checked",true);
        } else if (theme_index === 2) {
            $("body").attr("theme", "amber");
            $("#sideBarTheme .custom-radio input[value|='amber']").prop("checked",true);
        } else if (theme_index === 2) {
            $("body").attr("theme", "mint");
            $("#sideBarTheme .custom-radio input[value|='mint']").prop("checked",true);
        } else if (theme_index === 3) {
            $("body").attr("theme", "salmon-pink");
            $("#sideBarTheme .custom-radio input[value|='salmon pink']").prop("checked",true);
        }
    } else {
        localStorage.setItem("namafile-theme-mode", 0);
    }
}

init_theme();

$("#sideBarTheme .them-mode .big-radio input[value|='light']").change(function () {
    let status = $("#sideBarTheme .them-mode .radio-group .light input").prop("checked");
    if (status) {
        $("body").attr("theme-mode", "light");
        localStorage.setItem("namafile-theme-mode", 1);
    }
})
$("#sideBarTheme .them-mode .big-radio input[value|='dark']").change(function () {
    let status = $("#sideBarTheme .them-mode .radio-group .dark input").prop("checked");
    if (status) {
        $("body").attr("theme-mode", "dark");
        localStorage.setItem("namafile-theme-mode", 0);
    }
})

$("#sideBarTheme .custom-radio input[value|='lavender']").change(function () {
    let status = $(this).prop("checked");
    if (status) {
        $("body").attr("theme", "lavender");
        __global_theme__ = __theme_colors__[0];
        localStorage.setItem("namafile-theme", 0);
        setSurferTheme();
        setThemeToCharts();
    }
})
$("#sideBarTheme .custom-radio input[value|='ice blue']").change(function () {
    let status = $(this).prop("checked");
    if (status) {
        $("body").attr("theme", "ice-blue");
        __global_theme__ = __theme_colors__[1];
        localStorage.setItem("namafile-theme", 1);
        setSurferTheme();
        setThemeToCharts()
    }
})
$("#sideBarTheme .custom-radio input[value|='amber']").change(function () {
    let status = $(this).prop("checked");
    if (status) {
        $("body").attr("theme", "amber");
        __global_theme__ = __theme_colors__[2];
        localStorage.setItem("namafile-theme", 2);
        setSurferTheme();
        setThemeToCharts()
    }
})
$("#sideBarTheme .custom-radio input[value|='mint']").change(function () {
    let status = $(this).prop("checked");
    if (status) {
        $("body").attr("theme", "mint");
        __global_theme__ = __theme_colors__[3];
        localStorage.setItem("namafile-theme", 3);
        setSurferTheme();
        setThemeToCharts()
    }
})
$("#sideBarTheme .custom-radio input[value|='salmon pink']").change(function () {
    let status = $(this).prop("checked");
    if (status) {
        $("body").attr("theme", "salmon-pink");
        __global_theme__ = __theme_colors__[4];
        localStorage.setItem("namafile-theme", 4);
        setSurferTheme();
        setThemeToCharts()
    }
})


$("#sideBarTheme .custom-radio h6").click(function () {
    $(this).siblings("input").click();
})

