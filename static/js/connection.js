let websocket = null;
let endOfData = false;
let getedCount = 0;
let storedCount = 0;
// ====================================================================================================================
// ------- websocket handler ------------------------------------------------------------------------------------------
// ====================================================================================================================
function init_connection() {
    if (websocket !== null) {
        websocket.close();
    }
    websocket = new WebSocket(__websocket_url__);
    websocket.onopen = function (evt) {
        console.log("open")
    };
    websocket.onclose = function (evt) {
        // init_connection();
        console.log("close")
    };
    websocket.onmessage = function (evt) {
        websocket_onmessage_handler(evt);
    };
    websocket.onerror = function (evt) {
        alert("Connection failed, can not connect to server, please try again later");
        console.log("error", evt);
    };
}

window.addEventListener('beforeunload', function (e) {
    websocke.close();
});

function send_inital_message() {
    let send_json = {};
    send_json["Video_addr"] = __video_url__ + __global_video_file_name__;
    for (let i = 0; i < __log_key_value_map__.length; i++) {
        let value = "0";
        if (__global_features_list__.includes(__log_key_value_map__[i].key)) {
            value = "1";
        }
        send_json[__log_key_value_map__[i].key] = value;
    }
    console.log(JSON.stringify(send_json));
    websocket.send("initialize:" + JSON.stringify(send_json));
}

// ====================================================================================================================
// ------- websocket given data handler (check is header or log data) -------------------------------------------------
// ====================================================================================================================
function websocket_onmessage_handler(evt) {
    let geted_data;
    try {
        geted_data = JSON.parse(evt.data);
    } catch (e) {
        geted_data = null;
    }
    if (geted_data === null) {
        endOfData = true;
    } else if (geted_data.type == "header") {
        add_video_imformation(geted_data.data)
    } else if (geted_data.type == "warning") {
        alert("Server Warning: " + geted_data.message)
        // let warn_element = $(".video-container .alert-warning");
        // warn_element.text(geted_data.message);
        // warn_element.css("display","block");
        // setTimeout(() => {
        //     warn_element.css("display","none");            
        // }, __backend_warning_display_timeout__);
    } else if (geted_data.type == "error") {
        alert("Server Error: " + geted_data.message)
        // let error_element = $(".video-container .alert-danger");
        // error_element.text(geted_data.message);
        // error_element.css("display","block");
        // setTimeout(() => {
        //     error_element.css("display","none");            
        // }, __backend_error_display_timeout__);
    } else {
        getedCount++;
        sotore_data_in_db(geted_data).then(function (result) {
            storedCount++;
            analysis_progress(geted_data.data_time);
            if (endOfData && getedCount === storedCount) {
                console.log("nnnnnnnnnnnnnnnnnnow");
                storedCount = 0;
                getedCount = 0;
                endOfData = false;
                __global_data_show_permision__ = true;
                end_of_analysis_handler();
                features_error_rendering();
                render_error_timeline();
                fe_set_range_to_view(0, __global_viewport_capacity__);
                add_error_logs(10);
            }
            console.log("stored", geted_data.data_time);
        });
    }
}

// init_connection()
