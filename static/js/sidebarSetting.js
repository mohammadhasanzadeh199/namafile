const new_setting = __log_key_value_map__.map((item) => ({ key: item.key, treshold: item.treshold }));

function initialize_setting() {
    $("#sideBarSetting .treshold-input-field:not(.source)").remove();
    let store_setting = localStorage.getItem("namafile_setting");
    if (store_setting && store_setting !== null) {
        let parse_setting = JSON.parse(store_setting);
        let extra = new_setting.filter((item) => !parse_setting.map((item2) => item2.key).includes(item.key));
        parse_setting = [...extra, ...parse_setting];
        let pure = parse_setting.filter((item) => new_setting.map((item2) => item2.key).includes(item.key));
        if (JSON.stringify(pure) !== store_setting) {
            localStorage.setItem("namafile_setting", JSON.stringify(pure));
        }
        __global_setting__ = pure;
    } else {
        localStorage.setItem("namafile_setting", JSON.stringify(new_setting));
        __global_setting__ = new_setting;
    }
    for (let i = 0; i < __global_setting__.length; i++) {
        let elem = $("#sideBarSetting .treshold-input-field.source").clone();
        elem.removeClass("source");
        elem.find("label").text(map_key_to_name(__global_setting__[i].key));
        elem.find("input").attr("key", __global_setting__[i].key);
        elem.find("input").val(__global_setting__[i].treshold);
        elem.find("input").on("input",function(e){
            settingInputHandler(e,__global_setting__[i].key);
        })
        elem.insertBefore($("#sideBarSetting .treshold-input-field.source"));
    }
}

initialize_setting();


$("#sideBarSetting .toolbar .save").click(function () {
    console.log(__global_setting__)
    localStorage.setItem("namafile_setting", JSON.stringify(__global_setting__));
})
$("#sideBarSetting .toolbar .reset").click(function () {
    localStorage.setItem("namafile_setting", JSON.stringify(new_setting));
    __global_setting__ = new_setting;
    initialize_setting();
})

function settingInputHandler(e,key){
    let value = Number(e.target.value);
    __global_setting__.find((item) => item.key === key).treshold = value;
    console.log(key,value,__global_setting__)
}