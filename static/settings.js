__log_key_value_map__ = [
    {
        key: "brightness",
        value: "Brightness",
        icon: "&#xe906",
        type: "video",
        treshold: 0.5
    },
    {
        key: "contrast",
        value: "Contrast",
        icon: "&#xe90d",
        type: "video",
        treshold: 0.5
    },
    {
        key: "chroma",
        value: "Chroma",
        icon: "&#xe907",
        type: "video",
        treshold: 0.5
    },
    {
        key: "colorfulness",
        value: "Colorfulness",
        icon: "&#xe90b",
        type: "video",
        treshold: 0.5
    },
    {
        key: "color_gamut",
        value: "Color gamut",
        icon: "&#xe90c",
        type: "video",
        treshold: 0.5
    },
    {
        key: "chroma_imbalance",
        value: "Chroma imbalance",
        icon: "&#xe908",
        type: "video",
        treshold: 0.5
    },
    {
        key: "loss_of_chroma",
        value: "Loss of chroma",
        icon: "&#xe912",
        type: "video",
        treshold: 0.5
    },
    {
        key: "blurriness",
        value: "Blurriness",
        icon: "&#xe905",
        type: "video",
        treshold: 0.5
    },
    {
        key: "blockiness",
        value: "Blockiness",
        icon: "&#xe903",
        type: "video",
        treshold: 0.5
    },
    {
        key: "noise_estimation",
        value: "Noise estimation",
        icon: "&#xe91a",
        type: "video",
        treshold: 0.5
    },
    {
        key: "stripe_noise",
        value: "Stripe noise",
        icon: "&#xe91f",
        type: "video",
        treshold: 0.5
    },
    {
        key: "scene_change",
        value: "Scene change",
        icon: "&#xe91e",
        type: "video",
        treshold: 0.5
    },
    {
        key: "blooming",
        value: "Bloominess",
        icon: "&#xe904",
        type: "video",
        treshold: 0.5
    },
    {
        key: "frame_freezing",
        value: "Frame freezing",
        icon: "&#xe910",
        type: "video",
        treshold: 0.5
    },
    {
        key: "black_frame",
        value: "Black frame",
        icon: "&#xe902",
        type: "video",
        treshold: 0.5
    },
    {
        key: "pillar_boxing",
        value: "Pillar boxing",
        icon: "&#xe91d",
        type: "video",
        treshold: 0.5
    },
    {
        key: "window_boxing",
        value: "Window boxing",
        icon: "&#xe921",
        type: "video",
        treshold: 0.5
    },
    {
        key: "letter_boxing",
        value: "Letter boxing",
        icon: "&#xe911",
        type: "video",
        treshold: 0.5
    },
    {
        key: "frame_drop",
        value: "Frame drop",
        icon: "&#xe90f",
        type: "video",
        treshold: 0.5
    },
    {
        //no icon
        key: "photosensitive_epilepsy",
        value: "Photo-sensitive Epilepsy",
        icon: "&#xe90e",
        type: "video",
        treshold: 0.5
    },
    {
        //no icon
        key: "saturated_red_epilepsy",
        value: "Saturated red epilepsy",
        icon: "&#xe90e",
        type: "video",
        treshold: 0.5
    },
    {
        key: "luminance_change",
        value: "Luminance change",
        icon: "&#xe916",
        type: "video",
        treshold: 0.5
    },
    {
        key: "momentary_loudness",
        value: "Momentary loudness",
        icon: "&#xe914",
        type: "audio",
        treshold: 0.5
    },
    {
        key: "integrated_loudness",
        value: "Integrated loudness",
        icon: "&#xe913",
        type: "audio",
        treshold: 0.5
    },
    {
        key: "shortterm_loudness",
        value: "Shortterm loudness",
        icon: "&#xe913",
        type: "audio",
        treshold: 0.5
    },
    {
        key: "loudness_range",
        value: "Loudness range unit(LRU)",
        icon: "&#xe913",
        type: "audio",
        treshold: 0.5
    },
    {
        key: "true_peak_level",
        value: "True peak level",
        icon: "&#xe913",
        type: "audio",
        treshold: 0.5
    },
    {
        //no icon
        key: "loudness_mismatch",
        value: "Loudness mismatch",
        icon: "&#xe913",
        type: "audio",
        treshold: 0.5
    },
    {
        key: "audio_contrast",
        value: "Audio contrast",
        icon: "&#xe901",
        type: "audio",
        treshold: 0.5
    },
    {
        key: "audio_constant",
        value: "Audio constant",
        icon: "&#xe900",
        type: "audio",
        treshold: 0.5
    },
    {
        //no icon
        key: "mute_detection",
        value: "Mute detection",
        icon: "&#xe918",
        type: "audio",
        treshold: 0.5
    },
    {
        key: "noise_detection",
        value: "Noise detection",
        icon: "&#xe919",
        type: "audio",
        treshold: 0.5
    },
    {
        key: "phase_distortion",
        value: "Phase distortion",
        icon: "&#xe91c",
        type: "audio",
        treshold: 0.5
    },
    {
        key: "phase_coherence",
        value: "Phase coherence",
        icon: "&#xe91b",
        type: "audio",
        treshold: 0.5
    },
    {
        key: "clipping_detection",
        value: "Clipping detection",
        icon: "&#xe90a",
        type: "audio",
        treshold: 0.5
    },
    {
        key: "thdn",
        value: "THDN",
        icon: "&#xe920",
        type: "audio",
        treshold: 0.5
    },
    {
        key: "sinad",
        value: "Sinad",
        icon: "&#xe906",
        type: "audio",
        treshold: 0.5
    },
    {
        key: "snr",
        value: "SNR",
        icon: "&#xe906",
        type: "audio",
        treshold: 0.5
    },
    {
        key: "mos",
        value: "MOS",
        icon: "&#xe917",
        type: "video",
        treshold: 0.5
    },
    {
        key: "duplicate",
        value: "Duplicate",
        icon: "&#xe906",
        type: "video",
        treshold: 0.5
    },
    {
        key: "clicks_and_pops",
        value: "Clicks and pops",
        icon: "&#xe909",
        type: "audio",
        treshold: 0.5
    },
    {
        key: "low_luminance",
        value: "Low luminance",
        icon: "&#xe915",
        type: "video",
        treshold: 0.5
    }
]

var __num_of_error_timeline_capacity__ = 150;
var __csv_file_capacity__ = 100;



//error handling
//server file reading
//header save
//select features flow
//import video loop
//hard rendering chart


// =============================================================================================================
// ============ url setting variables ==========================================================================
// =============================================================================================================

// ------------- streaming url of video player (dev setting) --------------------------------------------------- 
var __video_url__ = "C:\\Users\\Administrator\\namafile\\videos\\";
// ------------- websocket server url --------------------------------------------------------------------------
var __websocket_url__ = "ws://213.233.161.125:8007/";



// ------------- time limit for return value back to under treshold value --------------------------------------
var __log_waiting_time__ = 10;



// =============================================================================================================
// ============ export setting =================================================================================
// =============================================================================================================

// ------------- export file prefix name -----------------------------------------------------------------------
var __export_pre_name__ = "DATA";







// =============================================================================================================
// ============ text messages ==================================================================================
// =============================================================================================================
var __websocket_error_text__ = "Data server connection error. please wait while reconnecting ..."
var __init_pts_findig_text__ = "Please wait while syncing video and data ..."

